/*
 * generated by Xtext 2.26.0
 */
package co.uniandes.proyectogrado.ide;

import co.uniandes.proyectogrado.UniandesDslRuntimeModule;
import co.uniandes.proyectogrado.UniandesDslStandaloneSetup;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
public class UniandesDslIdeSetup extends UniandesDslStandaloneSetup {

	@Override
	public Injector createInjector() {
		return Guice.createInjector(Modules2.mixin(new UniandesDslRuntimeModule(), new UniandesDslIdeModule()));
	}
	
}
