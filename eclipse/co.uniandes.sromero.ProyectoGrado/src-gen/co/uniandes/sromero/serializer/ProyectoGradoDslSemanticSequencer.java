/*
 * generated by Xtext 2.26.0
 */
package co.uniandes.sromero.serializer;

import co.uniandes.sromero.proyectoGradoDsl.CloudTest;
import co.uniandes.sromero.proyectoGradoDsl.Domainmodel;
import co.uniandes.sromero.proyectoGradoDsl.Parser;
import co.uniandes.sromero.proyectoGradoDsl.ProyectoGradoDslPackage;
import co.uniandes.sromero.services.ProyectoGradoDslGrammarAccess;
import com.google.inject.Inject;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class ProyectoGradoDslSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private ProyectoGradoDslGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == ProyectoGradoDslPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case ProyectoGradoDslPackage.CLOUD_TEST:
				sequence_CloudTest(context, (CloudTest) semanticObject); 
				return; 
			case ProyectoGradoDslPackage.DOMAINMODEL:
				sequence_Domainmodel(context, (Domainmodel) semanticObject); 
				return; 
			case ProyectoGradoDslPackage.PARSER:
				sequence_Parser(context, (Parser) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * <pre>
	 * Contexts:
	 *     CloudTest returns CloudTest
	 *
	 * Constraint:
	 *     (
	 *         testName=ID 
	 *         platforms=STRING 
	 *         appPath=STRING 
	 *         testsPath=STRING 
	 *         cloudProjectID=STRING 
	 *         devicePoolID=STRING
	 *     )
	 * </pre>
	 */
	protected void sequence_CloudTest(ISerializationContext context, CloudTest semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__TEST_NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__TEST_NAME));
			if (transientValues.isValueTransient(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__PLATFORMS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__PLATFORMS));
			if (transientValues.isValueTransient(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__APP_PATH) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__APP_PATH));
			if (transientValues.isValueTransient(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__TESTS_PATH) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__TESTS_PATH));
			if (transientValues.isValueTransient(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__CLOUD_PROJECT_ID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__CLOUD_PROJECT_ID));
			if (transientValues.isValueTransient(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__DEVICE_POOL_ID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, ProyectoGradoDslPackage.Literals.CLOUD_TEST__DEVICE_POOL_ID));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getCloudTestAccess().getTestNameIDTerminalRuleCall_2_0(), semanticObject.getTestName());
		feeder.accept(grammarAccess.getCloudTestAccess().getPlatformsSTRINGTerminalRuleCall_5_0(), semanticObject.getPlatforms());
		feeder.accept(grammarAccess.getCloudTestAccess().getAppPathSTRINGTerminalRuleCall_8_0(), semanticObject.getAppPath());
		feeder.accept(grammarAccess.getCloudTestAccess().getTestsPathSTRINGTerminalRuleCall_11_0(), semanticObject.getTestsPath());
		feeder.accept(grammarAccess.getCloudTestAccess().getCloudProjectIDSTRINGTerminalRuleCall_14_0(), semanticObject.getCloudProjectID());
		feeder.accept(grammarAccess.getCloudTestAccess().getDevicePoolIDSTRINGTerminalRuleCall_17_0(), semanticObject.getDevicePoolID());
		feeder.finish();
	}
	
	
	/**
	 * <pre>
	 * Contexts:
	 *     Domainmodel returns Domainmodel
	 *
	 * Constraint:
	 *     cloudTest+=CloudTest+
	 * </pre>
	 */
	protected void sequence_Domainmodel(ISerializationContext context, Domainmodel semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * <pre>
	 * Contexts:
	 *     Parser returns Parser
	 *
	 * Constraint:
	 *     (vals+=CloudPlatforms vals+=CloudPlatforms*)
	 * </pre>
	 */
	protected void sequence_Parser(ISerializationContext context, Parser semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
}
