package co.uniandes.sromero.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import co.uniandes.sromero.services.ProyectoGradoDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalProyectoGradoDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Create'", "'Test'", "'For'", "'Platforms'", "'USING'", "'AppPath'", "','", "'TestsPath'", "'CloudProjectID'", "'DevicePoolID'", "'AWS'", "'GCP'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalProyectoGradoDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalProyectoGradoDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalProyectoGradoDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalProyectoGradoDsl.g"; }



     	private ProyectoGradoDslGrammarAccess grammarAccess;

        public InternalProyectoGradoDslParser(TokenStream input, ProyectoGradoDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Domainmodel";
       	}

       	@Override
       	protected ProyectoGradoDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDomainmodel"
    // InternalProyectoGradoDsl.g:64:1: entryRuleDomainmodel returns [EObject current=null] : iv_ruleDomainmodel= ruleDomainmodel EOF ;
    public final EObject entryRuleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainmodel = null;


        try {
            // InternalProyectoGradoDsl.g:64:52: (iv_ruleDomainmodel= ruleDomainmodel EOF )
            // InternalProyectoGradoDsl.g:65:2: iv_ruleDomainmodel= ruleDomainmodel EOF
            {
             newCompositeNode(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDomainmodel=ruleDomainmodel();

            state._fsp--;

             current =iv_ruleDomainmodel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // InternalProyectoGradoDsl.g:71:1: ruleDomainmodel returns [EObject current=null] : ( (lv_cloudTest_0_0= ruleCloudTest ) )* ;
    public final EObject ruleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject lv_cloudTest_0_0 = null;



        	enterRule();

        try {
            // InternalProyectoGradoDsl.g:77:2: ( ( (lv_cloudTest_0_0= ruleCloudTest ) )* )
            // InternalProyectoGradoDsl.g:78:2: ( (lv_cloudTest_0_0= ruleCloudTest ) )*
            {
            // InternalProyectoGradoDsl.g:78:2: ( (lv_cloudTest_0_0= ruleCloudTest ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalProyectoGradoDsl.g:79:3: (lv_cloudTest_0_0= ruleCloudTest )
            	    {
            	    // InternalProyectoGradoDsl.g:79:3: (lv_cloudTest_0_0= ruleCloudTest )
            	    // InternalProyectoGradoDsl.g:80:4: lv_cloudTest_0_0= ruleCloudTest
            	    {

            	    				newCompositeNode(grammarAccess.getDomainmodelAccess().getCloudTestCloudTestParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_cloudTest_0_0=ruleCloudTest();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getDomainmodelRule());
            	    				}
            	    				add(
            	    					current,
            	    					"cloudTest",
            	    					lv_cloudTest_0_0,
            	    					"co.uniandes.sromero.ProyectoGradoDsl.CloudTest");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleCloudTest"
    // InternalProyectoGradoDsl.g:100:1: entryRuleCloudTest returns [EObject current=null] : iv_ruleCloudTest= ruleCloudTest EOF ;
    public final EObject entryRuleCloudTest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCloudTest = null;


        try {
            // InternalProyectoGradoDsl.g:100:50: (iv_ruleCloudTest= ruleCloudTest EOF )
            // InternalProyectoGradoDsl.g:101:2: iv_ruleCloudTest= ruleCloudTest EOF
            {
             newCompositeNode(grammarAccess.getCloudTestRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCloudTest=ruleCloudTest();

            state._fsp--;

             current =iv_ruleCloudTest; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCloudTest"


    // $ANTLR start "ruleCloudTest"
    // InternalProyectoGradoDsl.g:107:1: ruleCloudTest returns [EObject current=null] : (otherlv_0= 'Create' otherlv_1= 'Test' ( (lv_testName_2_0= RULE_ID ) ) otherlv_3= 'For' otherlv_4= 'Platforms' ( (lv_platforms_5_0= RULE_STRING ) ) otherlv_6= 'USING' otherlv_7= 'AppPath' ( (lv_appPath_8_0= RULE_STRING ) ) otherlv_9= ',' otherlv_10= 'TestsPath' ( (lv_testsPath_11_0= RULE_STRING ) ) otherlv_12= ',' otherlv_13= 'CloudProjectID' ( (lv_cloudProjectID_14_0= RULE_STRING ) ) otherlv_15= ',' otherlv_16= 'DevicePoolID' ( (lv_devicePoolID_17_0= RULE_STRING ) ) ) ;
    public final EObject ruleCloudTest() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_testName_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_platforms_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_appPath_8_0=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token lv_testsPath_11_0=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token lv_cloudProjectID_14_0=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token lv_devicePoolID_17_0=null;


        	enterRule();

        try {
            // InternalProyectoGradoDsl.g:113:2: ( (otherlv_0= 'Create' otherlv_1= 'Test' ( (lv_testName_2_0= RULE_ID ) ) otherlv_3= 'For' otherlv_4= 'Platforms' ( (lv_platforms_5_0= RULE_STRING ) ) otherlv_6= 'USING' otherlv_7= 'AppPath' ( (lv_appPath_8_0= RULE_STRING ) ) otherlv_9= ',' otherlv_10= 'TestsPath' ( (lv_testsPath_11_0= RULE_STRING ) ) otherlv_12= ',' otherlv_13= 'CloudProjectID' ( (lv_cloudProjectID_14_0= RULE_STRING ) ) otherlv_15= ',' otherlv_16= 'DevicePoolID' ( (lv_devicePoolID_17_0= RULE_STRING ) ) ) )
            // InternalProyectoGradoDsl.g:114:2: (otherlv_0= 'Create' otherlv_1= 'Test' ( (lv_testName_2_0= RULE_ID ) ) otherlv_3= 'For' otherlv_4= 'Platforms' ( (lv_platforms_5_0= RULE_STRING ) ) otherlv_6= 'USING' otherlv_7= 'AppPath' ( (lv_appPath_8_0= RULE_STRING ) ) otherlv_9= ',' otherlv_10= 'TestsPath' ( (lv_testsPath_11_0= RULE_STRING ) ) otherlv_12= ',' otherlv_13= 'CloudProjectID' ( (lv_cloudProjectID_14_0= RULE_STRING ) ) otherlv_15= ',' otherlv_16= 'DevicePoolID' ( (lv_devicePoolID_17_0= RULE_STRING ) ) )
            {
            // InternalProyectoGradoDsl.g:114:2: (otherlv_0= 'Create' otherlv_1= 'Test' ( (lv_testName_2_0= RULE_ID ) ) otherlv_3= 'For' otherlv_4= 'Platforms' ( (lv_platforms_5_0= RULE_STRING ) ) otherlv_6= 'USING' otherlv_7= 'AppPath' ( (lv_appPath_8_0= RULE_STRING ) ) otherlv_9= ',' otherlv_10= 'TestsPath' ( (lv_testsPath_11_0= RULE_STRING ) ) otherlv_12= ',' otherlv_13= 'CloudProjectID' ( (lv_cloudProjectID_14_0= RULE_STRING ) ) otherlv_15= ',' otherlv_16= 'DevicePoolID' ( (lv_devicePoolID_17_0= RULE_STRING ) ) )
            // InternalProyectoGradoDsl.g:115:3: otherlv_0= 'Create' otherlv_1= 'Test' ( (lv_testName_2_0= RULE_ID ) ) otherlv_3= 'For' otherlv_4= 'Platforms' ( (lv_platforms_5_0= RULE_STRING ) ) otherlv_6= 'USING' otherlv_7= 'AppPath' ( (lv_appPath_8_0= RULE_STRING ) ) otherlv_9= ',' otherlv_10= 'TestsPath' ( (lv_testsPath_11_0= RULE_STRING ) ) otherlv_12= ',' otherlv_13= 'CloudProjectID' ( (lv_cloudProjectID_14_0= RULE_STRING ) ) otherlv_15= ',' otherlv_16= 'DevicePoolID' ( (lv_devicePoolID_17_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getCloudTestAccess().getCreateKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getCloudTestAccess().getTestKeyword_1());
            		
            // InternalProyectoGradoDsl.g:123:3: ( (lv_testName_2_0= RULE_ID ) )
            // InternalProyectoGradoDsl.g:124:4: (lv_testName_2_0= RULE_ID )
            {
            // InternalProyectoGradoDsl.g:124:4: (lv_testName_2_0= RULE_ID )
            // InternalProyectoGradoDsl.g:125:5: lv_testName_2_0= RULE_ID
            {
            lv_testName_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_testName_2_0, grammarAccess.getCloudTestAccess().getTestNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCloudTestRule());
            					}
            					setWithLastConsumed(
            						current,
            						"testName",
            						lv_testName_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_3, grammarAccess.getCloudTestAccess().getForKeyword_3());
            		
            otherlv_4=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_4, grammarAccess.getCloudTestAccess().getPlatformsKeyword_4());
            		
            // InternalProyectoGradoDsl.g:149:3: ( (lv_platforms_5_0= RULE_STRING ) )
            // InternalProyectoGradoDsl.g:150:4: (lv_platforms_5_0= RULE_STRING )
            {
            // InternalProyectoGradoDsl.g:150:4: (lv_platforms_5_0= RULE_STRING )
            // InternalProyectoGradoDsl.g:151:5: lv_platforms_5_0= RULE_STRING
            {
            lv_platforms_5_0=(Token)match(input,RULE_STRING,FOLLOW_9); 

            					newLeafNode(lv_platforms_5_0, grammarAccess.getCloudTestAccess().getPlatformsSTRINGTerminalRuleCall_5_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCloudTestRule());
            					}
            					setWithLastConsumed(
            						current,
            						"platforms",
            						lv_platforms_5_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_6=(Token)match(input,15,FOLLOW_10); 

            			newLeafNode(otherlv_6, grammarAccess.getCloudTestAccess().getUSINGKeyword_6());
            		
            otherlv_7=(Token)match(input,16,FOLLOW_8); 

            			newLeafNode(otherlv_7, grammarAccess.getCloudTestAccess().getAppPathKeyword_7());
            		
            // InternalProyectoGradoDsl.g:175:3: ( (lv_appPath_8_0= RULE_STRING ) )
            // InternalProyectoGradoDsl.g:176:4: (lv_appPath_8_0= RULE_STRING )
            {
            // InternalProyectoGradoDsl.g:176:4: (lv_appPath_8_0= RULE_STRING )
            // InternalProyectoGradoDsl.g:177:5: lv_appPath_8_0= RULE_STRING
            {
            lv_appPath_8_0=(Token)match(input,RULE_STRING,FOLLOW_11); 

            					newLeafNode(lv_appPath_8_0, grammarAccess.getCloudTestAccess().getAppPathSTRINGTerminalRuleCall_8_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCloudTestRule());
            					}
            					setWithLastConsumed(
            						current,
            						"appPath",
            						lv_appPath_8_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_9=(Token)match(input,17,FOLLOW_12); 

            			newLeafNode(otherlv_9, grammarAccess.getCloudTestAccess().getCommaKeyword_9());
            		
            otherlv_10=(Token)match(input,18,FOLLOW_8); 

            			newLeafNode(otherlv_10, grammarAccess.getCloudTestAccess().getTestsPathKeyword_10());
            		
            // InternalProyectoGradoDsl.g:201:3: ( (lv_testsPath_11_0= RULE_STRING ) )
            // InternalProyectoGradoDsl.g:202:4: (lv_testsPath_11_0= RULE_STRING )
            {
            // InternalProyectoGradoDsl.g:202:4: (lv_testsPath_11_0= RULE_STRING )
            // InternalProyectoGradoDsl.g:203:5: lv_testsPath_11_0= RULE_STRING
            {
            lv_testsPath_11_0=(Token)match(input,RULE_STRING,FOLLOW_11); 

            					newLeafNode(lv_testsPath_11_0, grammarAccess.getCloudTestAccess().getTestsPathSTRINGTerminalRuleCall_11_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCloudTestRule());
            					}
            					setWithLastConsumed(
            						current,
            						"testsPath",
            						lv_testsPath_11_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_12=(Token)match(input,17,FOLLOW_13); 

            			newLeafNode(otherlv_12, grammarAccess.getCloudTestAccess().getCommaKeyword_12());
            		
            otherlv_13=(Token)match(input,19,FOLLOW_8); 

            			newLeafNode(otherlv_13, grammarAccess.getCloudTestAccess().getCloudProjectIDKeyword_13());
            		
            // InternalProyectoGradoDsl.g:227:3: ( (lv_cloudProjectID_14_0= RULE_STRING ) )
            // InternalProyectoGradoDsl.g:228:4: (lv_cloudProjectID_14_0= RULE_STRING )
            {
            // InternalProyectoGradoDsl.g:228:4: (lv_cloudProjectID_14_0= RULE_STRING )
            // InternalProyectoGradoDsl.g:229:5: lv_cloudProjectID_14_0= RULE_STRING
            {
            lv_cloudProjectID_14_0=(Token)match(input,RULE_STRING,FOLLOW_11); 

            					newLeafNode(lv_cloudProjectID_14_0, grammarAccess.getCloudTestAccess().getCloudProjectIDSTRINGTerminalRuleCall_14_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCloudTestRule());
            					}
            					setWithLastConsumed(
            						current,
            						"cloudProjectID",
            						lv_cloudProjectID_14_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_15=(Token)match(input,17,FOLLOW_14); 

            			newLeafNode(otherlv_15, grammarAccess.getCloudTestAccess().getCommaKeyword_15());
            		
            otherlv_16=(Token)match(input,20,FOLLOW_8); 

            			newLeafNode(otherlv_16, grammarAccess.getCloudTestAccess().getDevicePoolIDKeyword_16());
            		
            // InternalProyectoGradoDsl.g:253:3: ( (lv_devicePoolID_17_0= RULE_STRING ) )
            // InternalProyectoGradoDsl.g:254:4: (lv_devicePoolID_17_0= RULE_STRING )
            {
            // InternalProyectoGradoDsl.g:254:4: (lv_devicePoolID_17_0= RULE_STRING )
            // InternalProyectoGradoDsl.g:255:5: lv_devicePoolID_17_0= RULE_STRING
            {
            lv_devicePoolID_17_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_devicePoolID_17_0, grammarAccess.getCloudTestAccess().getDevicePoolIDSTRINGTerminalRuleCall_17_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCloudTestRule());
            					}
            					setWithLastConsumed(
            						current,
            						"devicePoolID",
            						lv_devicePoolID_17_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCloudTest"


    // $ANTLR start "entryRuleCloudPlatforms"
    // InternalProyectoGradoDsl.g:275:1: entryRuleCloudPlatforms returns [String current=null] : iv_ruleCloudPlatforms= ruleCloudPlatforms EOF ;
    public final String entryRuleCloudPlatforms() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleCloudPlatforms = null;


        try {
            // InternalProyectoGradoDsl.g:275:54: (iv_ruleCloudPlatforms= ruleCloudPlatforms EOF )
            // InternalProyectoGradoDsl.g:276:2: iv_ruleCloudPlatforms= ruleCloudPlatforms EOF
            {
             newCompositeNode(grammarAccess.getCloudPlatformsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCloudPlatforms=ruleCloudPlatforms();

            state._fsp--;

             current =iv_ruleCloudPlatforms.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCloudPlatforms"


    // $ANTLR start "ruleCloudPlatforms"
    // InternalProyectoGradoDsl.g:282:1: ruleCloudPlatforms returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_AWS_0= ruleAWS | this_GCP_1= ruleGCP ) ;
    public final AntlrDatatypeRuleToken ruleCloudPlatforms() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        AntlrDatatypeRuleToken this_AWS_0 = null;

        AntlrDatatypeRuleToken this_GCP_1 = null;



        	enterRule();

        try {
            // InternalProyectoGradoDsl.g:288:2: ( (this_AWS_0= ruleAWS | this_GCP_1= ruleGCP ) )
            // InternalProyectoGradoDsl.g:289:2: (this_AWS_0= ruleAWS | this_GCP_1= ruleGCP )
            {
            // InternalProyectoGradoDsl.g:289:2: (this_AWS_0= ruleAWS | this_GCP_1= ruleGCP )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==21) ) {
                alt2=1;
            }
            else if ( (LA2_0==22) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalProyectoGradoDsl.g:290:3: this_AWS_0= ruleAWS
                    {

                    			newCompositeNode(grammarAccess.getCloudPlatformsAccess().getAWSParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AWS_0=ruleAWS();

                    state._fsp--;


                    			current.merge(this_AWS_0);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalProyectoGradoDsl.g:301:3: this_GCP_1= ruleGCP
                    {

                    			newCompositeNode(grammarAccess.getCloudPlatformsAccess().getGCPParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_GCP_1=ruleGCP();

                    state._fsp--;


                    			current.merge(this_GCP_1);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCloudPlatforms"


    // $ANTLR start "entryRuleAWS"
    // InternalProyectoGradoDsl.g:315:1: entryRuleAWS returns [String current=null] : iv_ruleAWS= ruleAWS EOF ;
    public final String entryRuleAWS() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleAWS = null;


        try {
            // InternalProyectoGradoDsl.g:315:43: (iv_ruleAWS= ruleAWS EOF )
            // InternalProyectoGradoDsl.g:316:2: iv_ruleAWS= ruleAWS EOF
            {
             newCompositeNode(grammarAccess.getAWSRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAWS=ruleAWS();

            state._fsp--;

             current =iv_ruleAWS.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAWS"


    // $ANTLR start "ruleAWS"
    // InternalProyectoGradoDsl.g:322:1: ruleAWS returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'AWS' ;
    public final AntlrDatatypeRuleToken ruleAWS() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalProyectoGradoDsl.g:328:2: (kw= 'AWS' )
            // InternalProyectoGradoDsl.g:329:2: kw= 'AWS'
            {
            kw=(Token)match(input,21,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getAWSAccess().getAWSKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAWS"


    // $ANTLR start "entryRuleGCP"
    // InternalProyectoGradoDsl.g:337:1: entryRuleGCP returns [String current=null] : iv_ruleGCP= ruleGCP EOF ;
    public final String entryRuleGCP() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleGCP = null;


        try {
            // InternalProyectoGradoDsl.g:337:43: (iv_ruleGCP= ruleGCP EOF )
            // InternalProyectoGradoDsl.g:338:2: iv_ruleGCP= ruleGCP EOF
            {
             newCompositeNode(grammarAccess.getGCPRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGCP=ruleGCP();

            state._fsp--;

             current =iv_ruleGCP.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGCP"


    // $ANTLR start "ruleGCP"
    // InternalProyectoGradoDsl.g:344:1: ruleGCP returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'GCP' ;
    public final AntlrDatatypeRuleToken ruleGCP() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalProyectoGradoDsl.g:350:2: (kw= 'GCP' )
            // InternalProyectoGradoDsl.g:351:2: kw= 'GCP'
            {
            kw=(Token)match(input,22,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getGCPAccess().getGCPKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGCP"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000100000L});

}