/*
 * generated by Xtext 2.26.0
 */
package co.uniandes.sromero.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class ProyectoGradoDslAntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("co/uniandes/sromero/parser/antlr/internal/InternalProyectoGradoDsl.tokens");
	}
}
