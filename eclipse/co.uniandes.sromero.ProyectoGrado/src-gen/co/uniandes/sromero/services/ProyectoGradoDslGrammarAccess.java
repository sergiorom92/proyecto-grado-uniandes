/*
 * generated by Xtext 2.26.0
 */
package co.uniandes.sromero.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import org.eclipse.xtext.Alternatives;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.Group;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.TerminalRule;
import org.eclipse.xtext.common.services.TerminalsGrammarAccess;
import org.eclipse.xtext.service.AbstractElementFinder;
import org.eclipse.xtext.service.GrammarProvider;

@Singleton
public class ProyectoGradoDslGrammarAccess extends AbstractElementFinder.AbstractGrammarElementFinder {
	
	public class DomainmodelElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "co.uniandes.sromero.ProyectoGradoDsl.Domainmodel");
		private final Assignment cCloudTestAssignment = (Assignment)rule.eContents().get(1);
		private final RuleCall cCloudTestCloudTestParserRuleCall_0 = (RuleCall)cCloudTestAssignment.eContents().get(0);
		
		//Domainmodel:
		//    (cloudTest+=CloudTest)*;
		@Override public ParserRule getRule() { return rule; }
		
		//(cloudTest+=CloudTest)*
		public Assignment getCloudTestAssignment() { return cCloudTestAssignment; }
		
		//CloudTest
		public RuleCall getCloudTestCloudTestParserRuleCall_0() { return cCloudTestCloudTestParserRuleCall_0; }
	}
	public class CloudTestElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "co.uniandes.sromero.ProyectoGradoDsl.CloudTest");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cCreateKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Keyword cTestKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cTestNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cTestNameIDTerminalRuleCall_2_0 = (RuleCall)cTestNameAssignment_2.eContents().get(0);
		private final Keyword cForKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Keyword cPlatformsKeyword_4 = (Keyword)cGroup.eContents().get(4);
		private final Assignment cPlatformsAssignment_5 = (Assignment)cGroup.eContents().get(5);
		private final RuleCall cPlatformsSTRINGTerminalRuleCall_5_0 = (RuleCall)cPlatformsAssignment_5.eContents().get(0);
		private final Keyword cUSINGKeyword_6 = (Keyword)cGroup.eContents().get(6);
		private final Keyword cAppPathKeyword_7 = (Keyword)cGroup.eContents().get(7);
		private final Assignment cAppPathAssignment_8 = (Assignment)cGroup.eContents().get(8);
		private final RuleCall cAppPathSTRINGTerminalRuleCall_8_0 = (RuleCall)cAppPathAssignment_8.eContents().get(0);
		private final Keyword cCommaKeyword_9 = (Keyword)cGroup.eContents().get(9);
		private final Keyword cTestsPathKeyword_10 = (Keyword)cGroup.eContents().get(10);
		private final Assignment cTestsPathAssignment_11 = (Assignment)cGroup.eContents().get(11);
		private final RuleCall cTestsPathSTRINGTerminalRuleCall_11_0 = (RuleCall)cTestsPathAssignment_11.eContents().get(0);
		private final Keyword cCommaKeyword_12 = (Keyword)cGroup.eContents().get(12);
		private final Keyword cCloudProjectIDKeyword_13 = (Keyword)cGroup.eContents().get(13);
		private final Assignment cCloudProjectIDAssignment_14 = (Assignment)cGroup.eContents().get(14);
		private final RuleCall cCloudProjectIDSTRINGTerminalRuleCall_14_0 = (RuleCall)cCloudProjectIDAssignment_14.eContents().get(0);
		private final Keyword cCommaKeyword_15 = (Keyword)cGroup.eContents().get(15);
		private final Keyword cDevicePoolIDKeyword_16 = (Keyword)cGroup.eContents().get(16);
		private final Assignment cDevicePoolIDAssignment_17 = (Assignment)cGroup.eContents().get(17);
		private final RuleCall cDevicePoolIDSTRINGTerminalRuleCall_17_0 = (RuleCall)cDevicePoolIDAssignment_17.eContents().get(0);
		
		//CloudTest:
		//    'Create' 'Test' testName=ID 'For' 'Platforms' platforms=STRING
		//           'USING'      'AppPath' appPath=STRING ','
		//                     'TestsPath' testsPath=STRING ','
		//                     'CloudProjectID' cloudProjectID= STRING ','
		//                     'DevicePoolID' devicePoolID=STRING;
		@Override public ParserRule getRule() { return rule; }
		
		//'Create' 'Test' testName=ID 'For' 'Platforms' platforms=STRING
		//       'USING'      'AppPath' appPath=STRING ','
		//                 'TestsPath' testsPath=STRING ','
		//                 'CloudProjectID' cloudProjectID= STRING ','
		//                 'DevicePoolID' devicePoolID=STRING
		public Group getGroup() { return cGroup; }
		
		//'Create'
		public Keyword getCreateKeyword_0() { return cCreateKeyword_0; }
		
		//'Test'
		public Keyword getTestKeyword_1() { return cTestKeyword_1; }
		
		//testName=ID
		public Assignment getTestNameAssignment_2() { return cTestNameAssignment_2; }
		
		//ID
		public RuleCall getTestNameIDTerminalRuleCall_2_0() { return cTestNameIDTerminalRuleCall_2_0; }
		
		//'For'
		public Keyword getForKeyword_3() { return cForKeyword_3; }
		
		//'Platforms'
		public Keyword getPlatformsKeyword_4() { return cPlatformsKeyword_4; }
		
		//platforms=STRING
		public Assignment getPlatformsAssignment_5() { return cPlatformsAssignment_5; }
		
		//STRING
		public RuleCall getPlatformsSTRINGTerminalRuleCall_5_0() { return cPlatformsSTRINGTerminalRuleCall_5_0; }
		
		//'USING'
		public Keyword getUSINGKeyword_6() { return cUSINGKeyword_6; }
		
		//'AppPath'
		public Keyword getAppPathKeyword_7() { return cAppPathKeyword_7; }
		
		//appPath=STRING
		public Assignment getAppPathAssignment_8() { return cAppPathAssignment_8; }
		
		//STRING
		public RuleCall getAppPathSTRINGTerminalRuleCall_8_0() { return cAppPathSTRINGTerminalRuleCall_8_0; }
		
		//','
		public Keyword getCommaKeyword_9() { return cCommaKeyword_9; }
		
		//'TestsPath'
		public Keyword getTestsPathKeyword_10() { return cTestsPathKeyword_10; }
		
		//testsPath=STRING
		public Assignment getTestsPathAssignment_11() { return cTestsPathAssignment_11; }
		
		//STRING
		public RuleCall getTestsPathSTRINGTerminalRuleCall_11_0() { return cTestsPathSTRINGTerminalRuleCall_11_0; }
		
		//','
		public Keyword getCommaKeyword_12() { return cCommaKeyword_12; }
		
		//'CloudProjectID'
		public Keyword getCloudProjectIDKeyword_13() { return cCloudProjectIDKeyword_13; }
		
		//cloudProjectID= STRING
		public Assignment getCloudProjectIDAssignment_14() { return cCloudProjectIDAssignment_14; }
		
		//STRING
		public RuleCall getCloudProjectIDSTRINGTerminalRuleCall_14_0() { return cCloudProjectIDSTRINGTerminalRuleCall_14_0; }
		
		//','
		public Keyword getCommaKeyword_15() { return cCommaKeyword_15; }
		
		//'DevicePoolID'
		public Keyword getDevicePoolIDKeyword_16() { return cDevicePoolIDKeyword_16; }
		
		//devicePoolID=STRING
		public Assignment getDevicePoolIDAssignment_17() { return cDevicePoolIDAssignment_17; }
		
		//STRING
		public RuleCall getDevicePoolIDSTRINGTerminalRuleCall_17_0() { return cDevicePoolIDSTRINGTerminalRuleCall_17_0; }
	}
	public class ParserElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "co.uniandes.sromero.ProyectoGradoDsl.Parser");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cCommaKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cValsAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cValsCloudPlatformsParserRuleCall_1_0 = (RuleCall)cValsAssignment_1.eContents().get(0);
		private final Group cGroup_2 = (Group)cGroup.eContents().get(2);
		private final Keyword cCommaKeyword_2_0 = (Keyword)cGroup_2.eContents().get(0);
		private final Assignment cValsAssignment_2_1 = (Assignment)cGroup_2.eContents().get(1);
		private final RuleCall cValsCloudPlatformsParserRuleCall_2_1_0 = (RuleCall)cValsAssignment_2_1.eContents().get(0);
		private final Keyword cCommaKeyword_3 = (Keyword)cGroup.eContents().get(3);
		
		//Parser: ','* vals+=CloudPlatforms (','+ vals+=CloudPlatforms)* ','*;
		@Override public ParserRule getRule() { return rule; }
		
		//','* vals+=CloudPlatforms (','+ vals+=CloudPlatforms)* ','*
		public Group getGroup() { return cGroup; }
		
		//','*
		public Keyword getCommaKeyword_0() { return cCommaKeyword_0; }
		
		//vals+=CloudPlatforms
		public Assignment getValsAssignment_1() { return cValsAssignment_1; }
		
		//CloudPlatforms
		public RuleCall getValsCloudPlatformsParserRuleCall_1_0() { return cValsCloudPlatformsParserRuleCall_1_0; }
		
		//(','+ vals+=CloudPlatforms)*
		public Group getGroup_2() { return cGroup_2; }
		
		//','+
		public Keyword getCommaKeyword_2_0() { return cCommaKeyword_2_0; }
		
		//vals+=CloudPlatforms
		public Assignment getValsAssignment_2_1() { return cValsAssignment_2_1; }
		
		//CloudPlatforms
		public RuleCall getValsCloudPlatformsParserRuleCall_2_1_0() { return cValsCloudPlatformsParserRuleCall_2_1_0; }
		
		//','*
		public Keyword getCommaKeyword_3() { return cCommaKeyword_3; }
	}
	public class CloudPlatformsElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "co.uniandes.sromero.ProyectoGradoDsl.CloudPlatforms");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cAWSParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cGCPParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		
		//CloudPlatforms:
		//    AWS | GCP;
		@Override public ParserRule getRule() { return rule; }
		
		//AWS | GCP
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//AWS
		public RuleCall getAWSParserRuleCall_0() { return cAWSParserRuleCall_0; }
		
		//GCP
		public RuleCall getGCPParserRuleCall_1() { return cGCPParserRuleCall_1; }
	}
	public class AWSElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "co.uniandes.sromero.ProyectoGradoDsl.AWS");
		private final Keyword cAWSKeyword = (Keyword)rule.eContents().get(1);
		
		//AWS:'AWS';
		@Override public ParserRule getRule() { return rule; }
		
		//'AWS'
		public Keyword getAWSKeyword() { return cAWSKeyword; }
	}
	public class GCPElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "co.uniandes.sromero.ProyectoGradoDsl.GCP");
		private final Keyword cGCPKeyword = (Keyword)rule.eContents().get(1);
		
		//GCP:'GCP';
		@Override public ParserRule getRule() { return rule; }
		
		//'GCP'
		public Keyword getGCPKeyword() { return cGCPKeyword; }
	}
	
	
	private final DomainmodelElements pDomainmodel;
	private final CloudTestElements pCloudTest;
	private final ParserElements pParser;
	private final CloudPlatformsElements pCloudPlatforms;
	private final AWSElements pAWS;
	private final GCPElements pGCP;
	
	private final Grammar grammar;
	
	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public ProyectoGradoDslGrammarAccess(GrammarProvider grammarProvider,
			TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaTerminals = gaTerminals;
		this.pDomainmodel = new DomainmodelElements();
		this.pCloudTest = new CloudTestElements();
		this.pParser = new ParserElements();
		this.pCloudPlatforms = new CloudPlatformsElements();
		this.pAWS = new AWSElements();
		this.pGCP = new GCPElements();
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("co.uniandes.sromero.ProyectoGradoDsl".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	
	
	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	//Domainmodel:
	//    (cloudTest+=CloudTest)*;
	public DomainmodelElements getDomainmodelAccess() {
		return pDomainmodel;
	}
	
	public ParserRule getDomainmodelRule() {
		return getDomainmodelAccess().getRule();
	}
	
	//CloudTest:
	//    'Create' 'Test' testName=ID 'For' 'Platforms' platforms=STRING
	//           'USING'      'AppPath' appPath=STRING ','
	//                     'TestsPath' testsPath=STRING ','
	//                     'CloudProjectID' cloudProjectID= STRING ','
	//                     'DevicePoolID' devicePoolID=STRING;
	public CloudTestElements getCloudTestAccess() {
		return pCloudTest;
	}
	
	public ParserRule getCloudTestRule() {
		return getCloudTestAccess().getRule();
	}
	
	//Parser: ','* vals+=CloudPlatforms (','+ vals+=CloudPlatforms)* ','*;
	public ParserElements getParserAccess() {
		return pParser;
	}
	
	public ParserRule getParserRule() {
		return getParserAccess().getRule();
	}
	
	//CloudPlatforms:
	//    AWS | GCP;
	public CloudPlatformsElements getCloudPlatformsAccess() {
		return pCloudPlatforms;
	}
	
	public ParserRule getCloudPlatformsRule() {
		return getCloudPlatformsAccess().getRule();
	}
	
	//AWS:'AWS';
	public AWSElements getAWSAccess() {
		return pAWS;
	}
	
	public ParserRule getAWSRule() {
		return getAWSAccess().getRule();
	}
	
	//GCP:'GCP';
	public GCPElements getGCPAccess() {
		return pGCP;
	}
	
	public ParserRule getGCPRule() {
		return getGCPAccess().getRule();
	}
	
	//terminal ID: '^'?('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	}
	
	//terminal INT returns ecore::EInt: ('0'..'9')+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	}
	
	//terminal STRING:
	//            '"' ( '\\' . /* 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' */ | !('\\'|'"') )* '"' |
	//            "'" ( '\\' . /* 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' */ | !('\\'|"'") )* "'"
	//        ;
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	}
	
	//terminal ML_COMMENT : '/*' -> '*/';
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	}
	
	//terminal SL_COMMENT : '//' !('\n'|'\r')* ('\r'? '\n')?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	}
	
	//terminal WS         : (' '|'\t'|'\r'|'\n')+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	}
	
	//terminal ANY_OTHER: .;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	}
}
