printMessage() {
				    echo "$(tput setaf 0)$(tput setab 2)$1$(tput sgr 0)"
				}
				
				function getJSONAttribute() {
				    echo $(echo $1 | jq -r $2)
				}
				
				function getJSONArrayLength() {
				    echo $(echo $1 | jq -r "$2 | length")
				}
				