---- GCP ----
echo "Bash version ${BASH_VERSION}..."
. $PWD/utils.sh

#gcloud components update
#gcloud auth login

# Project Setup From User Input
projectID="ABC1235"
applicationPath="My_App_Path.apk"
testPackagePath="testsPath.apk"
applicationType="ANDROID_APP"

gcloud config set project uniandes-testing
printMessage "Fetching available Devices....."

availableDevices=$(gcloud firebase test android models list --format="json" --filter="supportedVersionIds>=28")
arrayLength=$(getJSONArrayLength "$availableDevices" ".")

printMessage "lenght $arrayLength"

printMessage "Select your desired Devices"

# Iterate over available devices
for i in $(seq 0 $(($arrayLength - 1))); do
    device=$(getJSONAttribute "$availableDevices" ".[$i]")
    deviceId=$(getJSONAttribute "$device" ".id")
    deviceManufacturer=$(getJSONAttribute "$device" ".manufacturer")
    deviceName=$(getJSONAttribute "$device" ".name")
    deviceSupportedVersionIds=$(getJSONAttribute "$device" ".supportedVersionIds")
    echo $(printMessage "$i".) $deviceId $deviceManufacturer $deviceName, Versions: $deviceSupportedVersionIds
done

printMessage "Enter Devices Separated by space"

read -r selectedOptions

devices=""

for i in $selectedOptions; do
    device=$(getJSONAttribute "$availableDevices" ".[$i]")
    deviceId=$(getJSONAttribute "$device" ".id")
    deviceVersion=$(getJSONAttribute "$device" ".supportedVersionIds[-1]")
    devices="$devices --device model=$deviceId,version=$deviceVersion,locale=en,orientation=portrait"
done

echo $(printMessage "gcloud firebase test android run --type instrumentation --app $applicationPath --test $testPackagePath $devices")
exec $(gcloud firebase test android run --type instrumentation --app $applicationPath --test $testPackagePath $devices)

exit
