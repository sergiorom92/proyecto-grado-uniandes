aws configure
aws devicefarm create-project --name MyProjectName
# Capture Project ARN
aws devicefarm list-device-pools --arn arn:MyProjectARN
# Capture Device Pool ARN
aws devicefarm create-upload -–project-arn arn:MyProjectArn -–name MyAndroid.apk -–type ANDROID_APP
# Extract Upload ARN, URL
curl -T MyAndroid.apk "https://prod-us-west-2-uploads.s3-us-west-2.amazonaws.com/ExampleURL"
# Check Status until status SUCCEEDED
aws devicefarm get-upload –-arn arn:MyAppUploadARN

aws devicefarm create-upload –-project-arn arn:MyProjectARN -–name MyTests.apk –-type INSTRUMENTATION_TEST_SPEC
# Extract Upload ARN, URL
curl -T MyTests.zip "https://prod-us-west-2-uploads.s3-us-west-2.amazonaws.com/ExampleURL"
# Check Status until status SUCCEEDED
aws devicefarm get-upload –-arn arn:MyTestsUploadARN
# Run test
aws devicefarm schedule-run --project-arn arn:MyProjectARN --app-arn arn:MyAppUploadARN --device-pool-arn arn:MyDevicePoolARN --name MyTestRun --test type=APPIUM_JAVA_TESTNG,testPackageArn=arn:MyTestPackageARN
# Extract Run ARN
aws devicefarm get-run --arn arn:aws:devicefarm:us-west-2:111122223333:run:5e01a8c7-c861-4c0a-b1d5-12345runEXAMPLE
