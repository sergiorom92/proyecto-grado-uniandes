echo "Bash version ${BASH_VERSION}..."
. $PWD/utils.sh

# Project Setup From User Input
projectArn="arn:aws:devicefarm:us-west-2:185131866603:project:86cd0a0c-2d35-4537-bda1-18b5b602129d"
applicationPath="app-debug.apk"
testPackagePath="app-debug-androidTest.apk"
applicationType="ANDROID_APP"
testRunName="Automate_Script_Name"

# Fetch Available devicePools from AWS
printMessage "Fetching available DevicePools"
availableDevicePools=$(aws devicefarm list-device-pools --arn $projectArn)
arrayLength=$(getJSONArrayLength "$availableDevicePools" ".devicePools")
printMessage "Select your desired DevicePool"
# Iterate over available devicePools
for i in $(seq 0 $(($arrayLength - 1))); do
    deviceName=$(getJSONAttribute "$availableDevicePools" ".devicePools[$i].name")
    echo "$i. $deviceName"
done
# Capture desired devicePool from user
read -r selectedOption

selectedDevicePool=$(getJSONAttribute "$availableDevicePools" ".devicePools[$selectedOption]")
selectedDevicePoolArn=$(getJSONAttribute "$selectedDevicePool" ".arn")
selectedDevicePoolName=$(getJSONAttribute "$selectedDevicePool" ".name")

echo "You have selected" $(printMessage "$selectedOption. $selectedDevicePoolName")

echo $selectedDevicePool selectedDevicePoolArn
echo "DevicePool Arn:" $(printMessage "$selectedDevicePoolArn")

# Upload Application Package
applicationUpload=$(aws devicefarm create-upload --project-arn $projectArn --name $applicationPath --type $applicationType)
applicationUploadARN=$(getJSONAttribute "$applicationUpload" ".upload.arn")
applicationUploadURL=$(getJSONAttribute "$applicationUpload" ".upload.url")
echo "Application Upload Arn:" $(printMessage "$applicationUploadARN")
echo "Application Upload Url:" $(printMessage "$applicationUploadURL")
printMessage ".....Uploading Application....."
curl -T $applicationPath $applicationUploadURL
uploadResult=$(aws devicefarm get-upload --arn $applicationUploadARN)
uploadResultText=$(getJSONAttribute "$uploadResult" ".upload.status")
echo ".....Application Upload Result.....:" $(printMessage "$uploadResultText")

# Upload Application Test Package
testPackageUpload=$(aws devicefarm create-upload --project-arn $projectArn --name $testPackagePath --type INSTRUMENTATION_TEST_PACKAGE)
testPackageUploadARN=$(getJSONAttribute "$testPackageUpload" ".upload.arn")
testPackageUploadURL=$(getJSONAttribute "$testPackageUpload" ".upload.url")
echo "Test Package Upload Arn:" $(printMessage "$testPackageUploadARN")
echo "Test Package Upload Url:" $(printMessage "$testPackageUploadURL")
printMessage ".....Uploading Test Package....."
curl -T $testPackagePath $testPackageUploadURL
uploadResult=$(aws devicefarm get-upload --arn $testPackageUploadARN)
uploadResultText=$(getJSONAttribute "$uploadResult" ".upload.status")
echo ".....Test Package Upload Result....." $(printMessage "$uploadResultText")

# Wait for Packages to be processed
sleep 5

# Run Testing on AWS
testRun=$(aws devicefarm schedule-run --project-arn $projectArn --app-arn $applicationUploadARN --device-pool-arn $selectedDevicePoolArn --name $testRunName --test type=INSTRUMENTATION,testPackageArn=$testPackageUploadARN)
printMessage ".....Test RUN Result....."
echo $testRun

exit
