aws configure
aws devicefarm create-project --name MyProjectName
# Capture Project ARN
aws devicefarm list-device-pools --arn arn:aws:devicefarm:us-west-2:185131866603:project:86cd0a0c-2d35-4537-bda1-18b5b602129d
# Capture Device Pool ARN
aws devicefarm create-upload --project-arn arn:aws:devicefarm:us-west-2:185131866603:project:86cd0a0c-2d35-4537-bda1-18b5b602129d --name app-debug.apk --type ANDROID_APP
# Extract Upload ARN, URL
curl -T app-debug.apk "https://prod-us-west-2-uploads.s3-us-west-2.amazonaws.com/arn%3Aaws%3Adevicefarm%3Aus-west-2%3A185131866603%3Aproject%3A86cd0a0c-2d35-4537-bda1-18b5b602129d/uploads/arn%3Aaws%3Adevicefarm%3Aus-west-2%3A185131866603%3Aupload%3A86cd0a0c-2d35-4537-bda1-18b5b602129d/e19a19f2-e235-4319-b66c-881f7f5a2f70/app-debug.apk?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20220413T011754Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAUJHLTYS5AWNTRO6L%2F20220413%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Signature=6c47cdf28e1eed98117547a883bea48a3eeb1e52d583610fb65570c569069f36"
# Check Status until status SUCCEEDED
aws devicefarm get-upload --arn arn:aws:devicefarm:us-west-2:185131866603:upload:86cd0a0c-2d35-4537-bda1-18b5b602129d/e19a19f2-e235-4319-b66c-881f7f5a2f70

aws devicefarm create-upload --project-arn arn:aws:devicefarm:us-west-2:185131866603:project:86cd0a0c-2d35-4537-bda1-18b5b602129d --name app-debug-androidTest_final.apk --type INSTRUMENTATION_TEST_PACKAGE

# Extract Upload ARN, URL
curl -T app-debug-androidTest.apk "https://prod-us-west-2-uploads.s3-us-west-2.amazonaws.com/arn%3Aaws%3Adevicefarm%3Aus-west-2%3A185131866603%3Aproject%3A86cd0a0c-2d35-4537-bda1-18b5b602129d/uploads/arn%3Aaws%3Adevicefarm%3Aus-west-2%3A185131866603%3Aupload%3A86cd0a0c-2d35-4537-bda1-18b5b602129d/423084af-8020-4a31-be7b-c1fe807178a6/app-debug-androidTest_final.apk?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20220418T173251Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAUJHLTYS5AWNTRO6L%2F20220418%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Signature=c678862612bd57cd5baea6e02ef6ddecd3def4458b2da8aa60980ffb0aff69c8"

# Check Status until status SUCCEEDED
aws devicefarm get-upload --arn arn:aws:devicefarm:us-west-2:185131866603:upload:86cd0a0c-2d35-4537-bda1-18b5b602129d/423084af-8020-4a31-be7b-c1fe807178a6







aws devicefarm schedule-run --project-arn arn:aws:devicefarm:us-west-2:185131866603:project:86cd0a0c-2d35-4537-bda1-18b5b602129d --app-arn arn:aws:devicefarm:us-west-2:185131866603:upload:86cd0a0c-2d35-4537-bda1-18b5b602129d/e19a19f2-e235-4319-b66c-881f7f5a2f70 --device-pool-arn arn:aws:devicefarm:us-west-2::devicepool:082d10e5-d7d7-48a5-ba5c-b33d66efa1f5 --name MyTestRun --test '{"type": "INSTRUMENTATION","testPackageArn":"arn:aws:devicefarm:us-west-2:185131866603:upload:86cd0a0c-2d35-4537-bda1-18b5b602129d/423084af-8020-4a31-be7b-c1fe807178a6"}'

 --test testSpecArn=arn:aws:devicefarm:us-west-2::upload:4f8bddf6-7be5-11e8-adc0-fa7ae01bbebc,type=INSTRUMENTATION,testPackageArn=arn:aws:devicefarm:us-west-2:185131866603:upload:86cd0a0c-2d35-4537-bda1-18b5b602129d/5cbd3430-f34b-4e9f-aa14-5e802e074f8e

#Retry 
TEST_INFO="{\"type\":\"INSTRUMENTATION\",\"testPackageArn\":\"arn:aws:devicefarm:us-west-2:185131866603:upload:86cd0a0c-2d35-4537-bda1-18b5b602129d/5cbd3430-f34b-4e9f-aa14-5e802e074f8e\"}";

aws devicefarm schedule-run --project-arn arn:aws:devicefarm:us-west-2:185131866603:project:86cd0a0c-2d35-4537-bda1-18b5b602129d --app-arn arn:aws:devicefarm:us-west-2:185131866603:upload:86cd0a0c-2d35-4537-bda1-18b5b602129d/47cfdbe9-393c-4d61-9e80-aee6edf760db --device-pool-arn arn:aws:devicefarm:us-west-2::devicepool:082d10e5-d7d7-48a5-ba5c-b33d66efa1f5 --name RUN_NAME --test "$TEST_INFO"

# Extract Run ARN
aws devicefarm get-run --arn arn:aws:devicefarm:us-west-2:111122223333:run:5e01a8c7-c861-4c0a-b1d5-12345runEXAMPLE



aws devicefarm list-uploads --arn arn:aws:devicefarm:us-west-2:185131866603:project:86cd0a0c-2d35-4537-bda1-18b5b602129d


aws devicefarm get-upload --arn arn:aws:devicefarm:us-west-2::upload:4f8bddf6-7be5-11e8-adc0-fa7ae01bbebc

curl "https://prod-us-west-2-uploads-testspec.s3-us-west-2.amazonaws.com/public-yaml-files/instrumentation.yml?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20220407T051048Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAUJHLTYS5AWNTRO6L%2F20220407%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Signature=38a99d55a34031647a1d7bfe7c382a8b8b840a9a44de5a1457dde672dfda3ee1" > MyTestSpec.yml


aws devicefarm create-upload --name MyTestSpec.yml --type INSTRUMENTATION_TEST_SPEC --project-arn arn:aws:devicefarm:us-west-2:185131866603:project:86cd0a0c-2d35-4537-bda1-18b5b602129d


arn:aws:devicefarm:us-west-2:185131866603:upload:86cd0a0c-2d35-4537-bda1-18b5b602129d/40f9fe6b-63ee-48b7-83d2-8b4de369e4f9
https://prod-us-west-2-uploads-testspec.s3-us-west-2.amazonaws.com/arn%3Aaws%3Adevicefarm%3Aus-west-2%3A185131866603%3Aproject%3A86cd0a0c-2d35-4537-bda1-18b5b602129d/uploads/arn%3Aaws%3Adevicefarm%3Aus-west-2%3A185131866603%3Aupload%3A86cd0a0c-2d35-4537-bda1-18b5b602129d/40f9fe6b-63ee-48b7-83d2-8b4de369e4f9/MyTestSpec.yml?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20220407T051301Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAUJHLTYS5AWNTRO6L%2F20220407%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Signature=b5d80c09395cd483fca326c48da1ed91e35b0ccae20d3d19a57ebb15eca95e85

curl -T MyTestSpec.yml "https://prod-us-west-2-uploads-testspec.s3-us-west-2.amazonaws.com/arn%3Aaws%3Adevicefarm%3Aus-west-2%3A185131866603%3Aproject%3A86cd0a0c-2d35-4537-bda1-18b5b602129d/uploads/arn%3Aaws%3Adevicefarm%3Aus-west-2%3A185131866603%3Aupload%3A86cd0a0c-2d35-4537-bda1-18b5b602129d/40f9fe6b-63ee-48b7-83d2-8b4de369e4f9/MyTestSpec.yml?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20220407T051301Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAUJHLTYS5AWNTRO6L%2F20220407%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Signature=b5d80c09395cd483fca326c48da1ed91e35b0ccae20d3d19a57ebb15eca95e85"

aws devicefarm get-upload --arn arn:aws:devicefarm:us-west-2:185131866603:upload:86cd0a0c-2d35-4537-bda1-18b5b602129d/40f9fe6b-63ee-48b7-83d2-8b4de369e4f9




