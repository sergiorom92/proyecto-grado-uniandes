package com.example.myapplication

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var value: TextView
    private lateinit var plus: Button
    private lateinit var minus: Button
    private lateinit var reset: Button
    private var counterValue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        plus = findViewById(R.id.plusBtn)
        minus = findViewById(R.id.minusBtn)
        reset = findViewById(R.id.resetBtn)

        value = findViewById(R.id.value)

        plus.setOnClickListener {
            val newVal = "" + ++counterValue
            value.text = newVal
        }
        minus.setOnClickListener {
            val newVal = "" + --counterValue
            value.text = newVal
        }

        reset.setOnClickListener {
            counterValue = 0
            val newVal = "" + counterValue
            value.text = newVal
        }
    }

}
